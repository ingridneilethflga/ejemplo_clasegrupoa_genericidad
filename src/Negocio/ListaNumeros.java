/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.util.Arrays;

/**
 * Nuestra primera estructura : Vector--> ListaNumeros
 *
 * @author madarme
 */
public class ListaNumeros implements Comparable {

    private float[] numeros;

    public ListaNumeros() {
    }

    public ListaNumeros(int cant) {
        if (cant <= 0) {
            throw new RuntimeException("No se puede crear el vector:" + cant);
        }

        this.numeros = new float[cant];

    }

    /**
     * Adiciona un número en la posición i
     *
     * @param i indice donde se va a ingresar el dato
     * @param numeroNuevo el dato a ingresar
     */
    public void adicionar(int i, float numeroNuevo) {
        this.validar(i);
        this.numeros[i] = numeroNuevo;
    }

    public float getElemento(int i) {

        this.validar(i);
        return this.numeros[i];
    }

    private void validar(int i) {
        if (i < 0 || i >= this.numeros.length) {
            throw new RuntimeException("Índice fuera de rango:" + i);
        }
    }

    /*
        ES REDUNDANTE
    **/
    public void actualizar(int i, float numeroNuevo) {
        this.adicionar(i, numeroNuevo);
    }

    public float[] getNumeros() {
        return numeros;
    }

    public void setNumeros(float[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        String msg = "";
        /*
            Cuando sólo son recorridos, usamos el foreach
            for(T elemento:coleccion)
                hacer algo con elemento;
         */

        for (float dato : this.numeros) {
            msg += dato + "  ";
        }
        return msg;
    }

    public int length() {
        return this.numeros.length;
    }

    /**
     * Elimina la posición i del vector y REDIMENSIONA EL VECTOR
     *
     * @param i la posición a eliminar
     */
    public void eliminar(int i) {
        this.validar(i);

        for (int j = i; j < this.numeros.length - 1; j++) {
            this.numeros[i] = this.numeros[i + 1];
        }
        ListaNumeros c = new ListaNumeros(this.numeros.length - 1);
        for (int k = 0; k < this.numeros.length - 1; k++) {
            c.adicionar(k, this.numeros[k]);
        }
        this.numeros = new float[c.length()];

        for (int x = 0; x < this.numeros.length; x++) {
            this.numeros[x] = c.getElemento(x);
        }
    }

    /**
     * Ordena el vector por el método de la burbuja
     */
    public void ordenar_Burbuja() {
        int i, j;
        float aux;
        for (i = 0; i < this.numeros.length - 1; i++) {
            for (j = 0; j < this.numeros.length - i - 1; j++) {
                if (this.numeros[j + 1] < this.numeros[j]) {
                    aux = this.numeros[j + 1];
                    this.numeros[j + 1] = this.numeros[j];
                    this.numeros[j] = aux;
                }
            }
        }
    }

    /**
     * Ordena el vector por el método de selección
     */
    public void ordenar_Seleccion() {
        int i, j, pos;
        float menor, tmp;
        for (i = 0; i < this.numeros.length - 1; i++) {
            menor = this.numeros[i];
            pos = i;
            for (j = i + 1; j < this.numeros.length; j++) {
                if (this.numeros[j] < menor) {
                    menor = this.numeros[j];
                    pos = j;
                }
            }
            if (pos != i) {
                tmp = this.numeros[i];
                this.numeros[i] = this.numeros[pos];
                this.numeros[pos] = tmp;
            }
        }
    }

    /**
     * Retorna una lista de numeros con la unión de conjuntos de lista original
     * con lista dos.
     *
     * Se debe validar que la listanueva contenga UNICAMENTE LA CANTIDAD DE
     * CELDAS REQUERIDAS
     *
     * Ejemplo: this={3,4,5,6} y dos={3,4} ListaNueva = {3,4,5,6}
     *
     * @param dos una lista de numeros
     * @return una nueva lista con la unión de conjuntos
     */
    public ListaNumeros getUnion(ListaNumeros dos) {
        ListaNumeros c = new ListaNumeros(this.numeros.length + dos.length());
        for (int i = 0; i < this.numeros.length; i++) {
            c.adicionar(i, this.numeros[i]);
        }
        int aux1 = 0;
        for (int j = this.numeros.length; j < this.numeros.length + dos.length(); j++) {
            c.adicionar(j, dos.getElemento(aux1++));
        }
        c.ordenar_Seleccion();
        int aux = 0;
        for (int k = 0; k < c.length() - 1; k++) {
            if (c.getElemento(k) != c.getElemento(k + 1)) {
                c.actualizar(aux++, c.getElemento(k));
            }
        }

        c.actualizar(aux++, c.getElemento(c.length() - 1));
        ListaNumeros d = new ListaNumeros(aux);

        for (int x = 0; x < aux; x++) {
            d.adicionar(x, c.getElemento(x));
        }
        return d;

    }

    /**
     * Retorna una lista de numeros con la INTERSECCION de conjuntos de lista
     * original con lista dos.
     *
     * Se debe validar que la listanueva contenga UNICAMENTE LA CANTIDAD DE
     * CELDAS REQUERIDAS
     *
     * Ejemplo: this={3,4,5,6} y dos={3,4} ListaNueva = {3,4}
     *
     * @param dos una lista de numeros
     * @return una nueva lista con la unión de conjuntos
     */
    public ListaNumeros getInterseccion(ListaNumeros dos) {

        ListaNumeros c = new ListaNumeros(this.numeros.length + dos.length());
        for (int i = 0; i < this.numeros.length; i++) {
            c.adicionar(i, this.numeros[i]);
        }
        int aux1 = 0;
        for (int j = this.numeros.length; j < this.numeros.length + dos.length(); j++) {
            c.adicionar(j, dos.getElemento(aux1++));
        }
        c.ordenar_Seleccion();
        int aux = 0;
        for (int k = 0; k < c.length() - 1; k++) {
            if (c.getElemento(k) == c.getElemento(k + 1)) {
                c.actualizar(aux++, c.getElemento(k));
            }
        }

        c.actualizar(aux++, c.getElemento(c.length() - 1));
        ListaNumeros d = new ListaNumeros(aux - 1);

        for (int x = 0; x < aux - 1; x++) {
            d.adicionar(x, c.getElemento(x));
        }
        return d;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.numeros);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaNumeros other = (ListaNumeros) obj;

        if (this.numeros.length != other.numeros.length) {
            return false;
        }

        for (int i = 0; i < this.numeros.length; i++) {
            if (this.numeros[i] != other.numeros[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Una lista es mayor a otra si y solo si: 1. Tienen el mismo tamaño 2.
     * Todos los elementos de la lista1 son mayores a los de la lista 2 en orden
     * ( suponga que la lista 1 y 2 se encuentra ordenada)
     *
     * @param obj
     * @return
     */
    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
        if (obj == null) {
            throw new RuntimeException("No se puede comparar por que el segundo objeto es null");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("Objetos no son de la misma Clase");
        }
        final ListaNumeros other = (ListaNumeros) obj;

        this.ordenar_Seleccion();
        other.ordenar_Seleccion();
        int res = 0;
        if (this.length() != other.length()) {
            throw new RuntimeException("Las listas no tienen el mismo tamaño");
        } else {
            for (int i = 0; i < this.length() - 1; i++) {
                if (this.getElemento(i) < other.getElemento(i)) {
                    res++;
                }
            }
        }
        return res;
    }

}
